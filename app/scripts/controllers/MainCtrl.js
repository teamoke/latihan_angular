'use strict';

angular.module('latihanAngularApp')
	.controller('MainCtrl', ['$scope', '$filter', 'ListNameService',
		function ($scope, $filter, ListNameService) {

			function setListNameData(response) {
				$scope.items = response.data.listname;
				$scope.items2 = $scope.items;
			}
			ListNameService.getListNameService(setListNameData);

			$scope.doOverlay = function (data) {
				$scope.$broadcast('overlaydesc', data);
			}
}]);