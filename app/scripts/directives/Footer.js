'use strict';

angular.module('footerModule')

.directive('latihanFooter', ['$location', '$anchorScroll',
	function ($location, $anchorScroll) {
		return {
			templateUrl: 'views/components/_footer.html',
			controller: '',
			restrict: 'E',
			link: function (scope) {}
		}
}]);