'use strict';

angular.module('latihanAngularApp')

.directive('overlay', ['$location', '$anchorScroll',
	function ($location, $anchorScroll) {
		return {
			templateUrl: 'views/components/_descreption-overlay.html',
			controller: '',
			restrict: 'E',
			link: function (scope) {
				scope.$on('overlaydesc', function (event, data) {
					scope.name = data;
					$(document).foundation();
				});
			}
		}
}]);