'use strict';

angular.module('headerModule')

.directive('latihanHeader', ['$location', '$anchorScroll',
	function ($location, $anchorScroll) {
		return {
			templateUrl: 'views/components/_header.html',
			controller: '',
			restrict: 'E',
			link: function (scope) {}
		}
}]);
