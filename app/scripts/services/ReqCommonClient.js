'use strict';

angular.module('service')
.service('ReqCommonClient',['$http', function ($http) {
    return {
        getAPI: function(url) {
            var req = {
                method: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'json'
                },
                data: { test: 'test' },
            };
            return $http(req);
        }
    };
}]);
