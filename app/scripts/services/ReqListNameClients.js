'use strict';

angular.module('service')
.service('ReqListNameClients',['ReqCommonClient', function(ReqCommonClient) {
    return {
        getListName: function() {
            var url= 'scripts/resource/list_name.JSON';
            return ReqCommonClient.getAPI(url);
        }
    };
}]);
