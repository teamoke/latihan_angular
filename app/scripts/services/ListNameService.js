'use strict';

angular.module('service')
.service('ListNameService',['ReqListNameClients', function (ReqListNameClients) {
    return{
        getListNameService : function(callback){
            ReqListNameClients.getListName().then(function(response) {
                callback(response);
            });
        }
    };
}]);