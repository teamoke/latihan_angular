'use strict';

angular.module('headerModule', []);
angular.module('footerModule', []);
angular.module('service', []);


angular
.module('latihanAngularApp', [
  'ngRoute',
  'headerModule',
  'footerModule',
  'ui.router',
	'service'
])
.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');
  $stateProvider
  .state('home', {
    url: '/',
    templateUrl: 'views/pages/home.html',
    controller: 'MainCtrl'
  })
});
